---
layout: handbook-page-toc
title: "Website Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Working with issues, groups, and labels

* [How it all fits together](https://about.gitlab.com/handbook/marketing/#how-it-all-fits-together)

## Issue labels

**At a minimum, website related issues should have the label `mktg-website` applied in order to populate appropriate boards.**

They should also have a label for your team and/or subject matter (ex: `blog`, `Digital Marketing`, `SEO`). These labels need to exist in either the root `GitLab.com` group or the `www-gitlab-com` repository.

Issues should follow the [standard marketing status labels](/handbook/marketing/#boards-and-labels) flow labels, with a few additions.

* `mktg-status::triage` this work is in the pre-planning stage. We're still discussing what to do.
* `mktg-status::plan` this work is in the planning stage. We know what we want to do but don't know how we want to do it yet.
* `mktg-status::design` this issue needs some prototyping or other UX designs before we know where we're going.
* `mktg-status::groomed` this issue has been planned and detailed. We know what needs to be done. We can start building it.
* `mktg-status::wip` this issue is actively being worked on.
* `mktg-status::blocked` something is blocking progress on this issue.
* `mktg-status::review` enough work has been completed that this is ready for review and approval.
* `mktg-status::scheduled` this issue cannot be merged until a scheduled date but the work is complete and approved.

Examples of optional labels include:

* `OKR`
* `outsourceable`

## Issue boards

* [Overall](https://gitlab.com/groups/gitlab-com/-/boards/1472883)
* [Blocked](https://gitlab.com/groups/gitlab-com/-/boards/1485169)
* [Bugs](https://gitlab.com/groups/gitlab-com/-/boards/1483331)
* [CMO](https://gitlab.com/groups/gitlab-com/-/boards/1486533)
* [Debt](https://gitlab.com/groups/gitlab-com/-/boards/1485186)
* [OKR](https://gitlab.com/groups/gitlab-com/-/boards/1483333)
* [Priority](https://gitlab.com/groups/gitlab-com/-/boards/1483370)
* [Team Dev](https://gitlab.com/groups/gitlab-com/-/boards/1485124)
* [Time Sensitive](https://gitlab.com/groups/gitlab-com/-/boards/1485208)
* [All Remote](https://gitlab.com/groups/gitlab-com/-/boards/1485066)
* [Analyst Relations](https://gitlab.com/groups/gitlab-com/-/boards/1485071)
* [Blog](https://gitlab.com/groups/gitlab-com/-/boards/1483337)
* [Content Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1483354)
* [Corporate Events](#)
* [Corporate Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485085)
* [Digital Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485086)
* [Marketing Ops](https://gitlab.com/groups/gitlab-com/-/boards/1485111)
* [Product Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485129)
* [Recruiting](https://gitlab.com/groups/gitlab-com/-/boards/1485175)
* [Strategic Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1489415)
* [Social](https://gitlab.com/groups/gitlab-com/-/boards/1485179)
* [Technical Evangelism](https://gitlab.com/groups/gitlab-com/-/boards/1485182)
