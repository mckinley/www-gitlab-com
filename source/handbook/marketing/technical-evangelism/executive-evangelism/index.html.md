---
layout: handbook-page-toc
title: "Executive Technical Evangelism"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Executives at GitLab speak on a number of different subjects, including technical ones that align with and support the initiatives of the Technical Evangelism and Corporate Marketing teams.
Requesting an Executive keynote or otherwise participate in an event is done in [collaboration with the EBA Team](/handbook/eba/#meeting-request-requirements).
Wherever possible, the ask of an executive to keynote or speak should include a suggestion of the content to be presented. 

## Content Creation

* DRI for Exec evangelism in general: Exec appointed person on a case-by-case basis 
* DRI for CEO evangelism: Member of the Office of the CEO (can be the Chief of Staff, the Internal Data Consultant, or someone else the CEO designated for a particular talk)

### Process
* The technical evangelism team creates an issue in the Corporate Marketing project [using the label of `sid-evangelism`](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues?label_name%5B%5D=sid-evangelism)
* Where there is content alignment, the technical evangelism team provides the thesis of the talk along with an outline in slide deck form
* The DRI collaborates on this thesis and outline and then presents it to the Exec for approval
* The DRI takes over slide deck creation with design support from our PR firm as necessary (one week lead time needed to acquire their resources)

## Content Review

* The EBA to the Exec is responsible for scheduling an initial set of practice sessions with the content DRI, the Exec, and a technical evangelism team member. It is the EBA's discretion based on experience with the Exec and their speaking engagements in the past to decide how many times to set up. 
* The call attendees are responsible to communicate with the EBA if they need more or less sessions ASAP

## Stock Content

Some executives have delivered talks that we can reference as a starting point for new speaking opportunities. 

### CEO

* Open Core Business Models
* Multicloud Maturity Model
* Remote Work/ The Future of Work



